import random
import math
import copy
def nodeName(nodeAmount,i):
        lst=[' ' for i in range(int(math.log(nodeAmount,27)//1)+1)]
        count=1
        while True :
            mod=i%27
            i=i//27
        
            lst[-1*count]=chr(64+mod)
            count=count+1
            if(i==0):
                break
        return ''.join(['@' if x==' ' else x for x in lst])
class OperationCount:
    def __init__(self):
        self.r=0
        self.w=0
        self.e=0
        self.h=0
        self.s=0
    def __str__(self):
        return "Read : "+str(self.r)+"\nWrite : "+str(self.w)+"\nEvaluate : "+str(self.e)+"\nHeuristic : "+str(self.h)+"\nSort : "+str(self.s)        
    
class Graph:
    class Node:
        def __init__(self,name,x,y):
            self.name=name
            self.posX=x
            self.posY=y
            self.edges=[]
        def addEdge(self,edge):
            self.edges.append(edge)
        def popEdge(self,edge):
            self.edges.remove(edge)
        def pairNodes(self):
            nodes=[]
            costs=[]
            for i in self.edges:
                nodes.append(i.pairNode(self))
                costs.append(i.weight)
            return nodes
        def heuristic(self,other):
            return ((self.posX - other.posX)** 2 + (self.posY - other.posY)** 2)** 0.5
            
    class Edge:
        def pairNode(self,node):
            if self.node1==node:
                return self.node2
            return self.node1
        def __init__(self,node1,node2,weight,existenceProb):
            self.node1 = node1
            self.node2 = node2
            self.weight = weight
            self.existenceProb = existenceProb
            self.existence = existenceProb > random.random()
    def __init__(self, nodeAmount, probEdgeAmount, edgeProbL, edgeProbH, costL, costH):
        #Edge existence ranges [edgeProbL,edgeProbH]
        #Edge cost ranges [costL,costH]
        self.solution=[]
        self.nodes=[]
        self.edges=[]
        i=1
        positions=set()
        while (i<=nodeAmount):
            x,y=random.randint(1,10),random.randint(0,9)
            while( (x,y) in positions):
                x,y=random.randint(1,10),random.randint(0,9)
            node = self.Node(nodeName(nodeAmount,i),x,y)
            self.nodes.append(node)
            positions.add((x,y))
            i+=1
        nodes=self.nodes
        length=len(nodes)
        while (probEdgeAmount > 0):
            j = random.randint(0,length-1)
            node = nodes[j]
            pair = nodes[randomExclude(0,length-1,j)]
            cost = random.randint(costL,costH)
            if(cost >= node.heuristic(pair)):
                edge = self.Edge(node,pair,cost,random.uniform(edgeProbL,edgeProbH))
                node.addEdge(edge)
                pair.addEdge(edge)
                self.edges.append(edge)
                probEdgeAmount = probEdgeAmount - 1
    def setSolution(self,solution):
        self.solution=solution
        
def randomExclude(low,high,exclude):
    x=random.randint(low,high)
    while(x==exclude):
        x=random.randint(low,high)
    return x
def lwa(graph):
    oc=OperationCount()
    current = graph.nodes[0]
    oc.w+=1
    end = graph.nodes[-1]
    oc.r+=1
    path = aStar(current,end,oc)
    oc.r+=1#if below
    if (path==[]):
        return False,None
    realPath=[]
    oc.w+=1#var assign
    while (not current==end and path!=[]):
        oc.r+=2#while check
        oc.r+=1#while check
        nextEdge=path.pop(0)
        oc.r+=1
        nextNode=nextEdge.pairNode(current)
        oc.r+=1#read pairNode
        oc.e+=1#Evaluate below
        if (nextEdge.existence):
            realPath.append(nextEdge)
            oc.w+=1
            current=nextNode
            oc.w+=1
        else:               
            current.popEdge(nextEdge)
            oc.w+=1
            nextNode.popEdge(nextEdge)
            oc.w+=1
            path = aStar(current,end,oc)
            oc.r+=1
            if (path==[]):
                return False,None
    return realPath,oc
def aStar(start,end,oc):
    traveledNodes=[]
    oc.w+=1
    sortedNodes = [[start,0,0,[]]]
    oc.w+=1
    while(len(sortedNodes)>0):
        oc.r+=1#while check
        current=sortedNodes.pop(0)
        currentNode = current[0]
        #currentFCost = current[1] (heuristic + real cost)
        currentCost = current[2]
        currentPath = current[3]
        oc.r+=4
        traveledNodes.append(currentNode)
        oc.w+=1
        oc.r+=1
        if(currentNode==end):
            return currentPath
        
        for edge in currentNode.edges:
            oc.r+=1#for check
            nextNode = edge.pairNode(currentNode)
            oc.r+=1
            flag=True
            oc.w+=1
            for i in traveledNodes:
                oc.r+=2
                if nextNode==i:
                    flag=False
            if(not flag):
                continue
            h = start.heuristic(nextNode)
            oc.h+=1
            oc.w+=1
            g = currentCost+edge.weight
            oc.w+=1
            sortedNodes.append([nextNode,g+h,g,currentPath+[edge]])
            oc.w+=1
        sortedNodes.sort(key = lambda x: x[1]) #sortBy FCost
        oc.s+=1
    return []

def esp(graph):
    oc=OperationCount()
    graphEdgeAmount=len(graph.edges)
    oc.r+=1
    checkOrder=[]
    oc.w+=1
    n=graphEdgeAmount**2-1
    count=0
    
    for i in range(0,2**graphEdgeAmount-1):
        count+=1
        oc.r+=1
        binList = binaryGet(i,graphEdgeAmount,oc)
        forbidEdges=[]
        for j in range(len(binList)):
            oc.r+=1
            if(not binList[j]):
                oc.w+=1
                forbidEdges.append(graph.edges[j])
        path,cost,chance = modifiedAStar(graph.nodes[0],graph.nodes[-1],forbidEdges,oc)
        flag=False
        oc.w+=4
        for j in checkOrder:
            oc.r+=1
            if j[0]==path:
                flag=True
                oc.w+=1
                break
        oc.r+=1
        if(flag):
            continue
        else:
            oc.w+=1
            checkOrder.append([path,cost,chance])
    oc.s+=1
    evaluated=[]
    checkOrder.sort(key = lambda x: x[2], reverse=True)
    for pathObj in checkOrder:
        oc.r+=1
        path = pathObj[0]
        oc.r+=2
        if(path==[]):
            continue
        oc.w+=1
        flag = True
        for edge in path:
            oc.r+=1
            if edge not in evaluated:
                oc.e+=1
                oc.w+=1
                evaluated.append(edge)
            if(not edge.existence):
                flag=False
                oc.w+=1
                break
        oc.r+=1
        if(flag):
            return pathObj[0],oc
    return False,None
        
        
def binaryGet(count, length, oc):
    binary = str(bin(count))[2:]
    oc.w+=1
    binaryList=[]
    oc.w+=1
    for i in range(len(binary)):
        binaryList.append(bool(int((binary[i]))))
        oc.r+=2
        oc.w+=1
    return [False for i in range(length-len(binary))] + binaryList
def modifiedAStar(start,end,forbidEdges,oc):    
    traveledNodes=[]
    sortedNodes = [[start,0,0,[],1]]
    oc.w+=2
    while(len(sortedNodes)>0):
        current=sortedNodes.pop(0)
        currentNode = current[0]
        #currentFCost = current[1] (heuristic + real cost)
        currentCost = current[2]
        currentPath = current[3]
        currentChance = current[4]
        oc.r+=5
        traveledNodes.append(currentNode)
        oc.w+=1
        oc.r+=1
        if(currentNode==end):
            return currentPath,currentCost,currentChance
        for edge in currentNode.edges:
            oc.r+=1
            oc.r+=1#if
            if (edge not in forbidEdges):
                nextNode = edge.pairNode(currentNode)
                oc.r+=1
                flag=False
                oc.w+=1
                for i in traveledNodes:
                    oc.r+=1 #if
                    if i==nextNode:
                        flag=True
                        oc.w+=1
                        break
                oc.r+=1
                if(flag):    
                    continue
                h = start.heuristic(nextNode)
                oc.h+=1
                g = currentCost+edge.weight
                oc.w+=1
                chance = edge.existenceProb * currentChance
                oc.w+=1
                sortedNodes.append([nextNode,g+h,g,currentPath+[edge],chance])
                oc.w+=1
                sortedNodes.sort(key = lambda x: x[1]) #sortBy FCost
    return [], float('inf'), 0

def pathCost(path):
    acc=0
    for i in path:
        acc+= i.weight
    return 'Cost : '+str(acc)
